package com.zhou.base.listener

import android.view.View
import com.zhou.base.utils.ToolsUtil

/**
 * Created by zhou on 16/12/14.
 */
abstract class NoDoubleClickListener : View.OnClickListener {
    override fun onClick(v: View) {
        if (ToolsUtil.isFastDoubleClick) {
            return
        }
        onNoDoubleClick(v)
    }

    abstract fun onNoDoubleClick(v: View)
}