package com.zhou.base.listener

import android.view.View
import androidx.recyclerview.widget.RecyclerView

interface OnItemClickListener {
    fun onItemClick(view: View?, holder: RecyclerView.ViewHolder?, position: Int)
    fun onItemLongClick(view: View?, holder: RecyclerView.ViewHolder?, position: Int): Boolean
}