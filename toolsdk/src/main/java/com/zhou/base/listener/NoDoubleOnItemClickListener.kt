package com.zhou.base.listener

import android.view.View
import android.widget.AdapterView
import com.zhou.base.utils.ToolsUtil

/**
 * Created by zhouruiyong on 16/12/14.
 */
abstract class NoDoubleOnItemClickListener : AdapterView.OnItemClickListener {
    override fun onItemClick(
        adapterView: AdapterView<*>?,
        view: View,
        position: Int,
        l: Long
    ) {
        if (ToolsUtil.isFastDoubleClick) {
            return
        }
        onNoDoubleItemClick(adapterView, view, position, l)
    }

    abstract fun onNoDoubleItemClick(
        adapterView: AdapterView<*>?,
        view: View?,
        position: Int,
        id: Long
    )
}