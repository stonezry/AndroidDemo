package com.zhou.base.utils

/**
 * Created by zhou on 17/3/20.
 */
object ToolsUtil {
    //防止连续点击
    /** 判断是否是快速点击  */
    private var lastClickTime: Long = 0
    val isFastDoubleClick: Boolean
        get() {
            val time = System.currentTimeMillis()
            val timeD = time - lastClickTime
            if (timeD < 500) {
                return true
            }
            lastClickTime = time
            return false
        }
}