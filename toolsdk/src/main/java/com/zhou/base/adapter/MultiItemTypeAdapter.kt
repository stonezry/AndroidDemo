package com.zhou.base.adapter

import android.content.Context
import android.view.View
import android.view.View.OnLongClickListener
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.zhou.base.adapter.RViewHolder.Companion.createViewHolder
import com.zhou.base.listener.NoDoubleClickListener


open class MultiItemTypeAdapter<T>(protected var mContext: Context, var datas: List<T>) : RecyclerView.Adapter<RViewHolder>() {
    protected var itemViewDelegate: ItemViewDelegate<T>? = null
    protected var mOnItemClickListener: OnItemClickListener? = null

    override fun getItemViewType(position: Int): Int {
        //根据传入adapter来判断是否有数据
        return if (datas.size > 0) NOT_EMPTY_VIEW else EMPTY_VIEW
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RViewHolder {
        val layoutId = itemViewDelegate!!.getItemViewLayoutId(viewType)
        val holder = createViewHolder(mContext, parent, layoutId)
        onViewHolderCreated(holder, holder.convertView, viewType)
        setListener(parent, holder, viewType)
        return holder
    }

    fun onViewHolderCreated(
        holder: RViewHolder?,
        itemView: View?,
        viewType: Int
    ) {
    }

    fun convert(holder: RViewHolder, t: T) {
        itemViewDelegate!!.convert(holder, t, holder.adapterPosition)
    }

    fun convertEmptyView(holder: RViewHolder?) {}
    protected fun isEnabled(viewType: Int): Boolean {
        return viewType != EMPTY_VIEW
    }

    protected fun setListener(
        parent: ViewGroup?,
        viewHolder: RViewHolder,
        viewType: Int
    ) {
        if (!isEnabled(viewType)) return
        viewHolder.convertView.setOnClickListener(object : NoDoubleClickListener() {
            override fun onNoDoubleClick(v: View) {
                val position = viewHolder.adapterPosition
                mOnItemClickListener?.onItemClick(v, viewHolder, position)
            }
        })
        viewHolder.convertView.setOnLongClickListener(OnLongClickListener { v ->

            mOnItemClickListener?.let {
                val position = viewHolder.adapterPosition
                return@OnLongClickListener it.onItemLongClick(v, viewHolder, position)
            }
            false
        })
    }

    override fun onBindViewHolder(holder: RViewHolder, position: Int) {
        if (getItemViewType(position) != EMPTY_VIEW) {
            convert(holder, datas[position])
        } else {
            convertEmptyView(holder)
        }
    }

    override fun getItemCount(): Int {
        return if (isEmptyViewShowed) {
            if (datas.isNotEmpty()) datas.size else 1
        } else {
            if (datas.isNotEmpty()) datas.size else 0
        }
    }

    fun setItemViewDelegate(delegate: ItemViewDelegate<T>?): MultiItemTypeAdapter<*> {
        itemViewDelegate = delegate
        return this
    }

    interface OnItemClickListener {
        fun onItemClick(
            view: View,
            holder: ViewHolder,
            position: Int
        )

        fun onItemLongClick(
            view: View,
            holder: ViewHolder,
            position: Int
        ): Boolean
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener?) {
        mOnItemClickListener = onItemClickListener
    }

    open val isEmptyViewShowed: Boolean
        get() = true

    companion object {
        const val EMPTY_VIEW = 0
        const val NOT_EMPTY_VIEW = 1
    }

}