package com.zhou.base.adapter

interface ItemViewDelegate<T> {
    fun getItemViewLayoutId(viewType: Int): Int
    fun isForViewType(item: T, position: Int): Boolean
    fun convert(holder: RViewHolder, t: T, position: Int)
}