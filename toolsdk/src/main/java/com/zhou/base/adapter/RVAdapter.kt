package com.zhou.base.adapter

import android.content.Context
import com.zhou.toolsdk.R

/**
 *
 * @fuction 通用Recylerview Adapter
 * @date 2019/5/22
 * @author zhouruiyong
 */
abstract class RVAdapter<T>(
    mContext: Context,
    protected var mLayoutId: Int,
    mDatas: List<T>
) : MultiItemTypeAdapter<T>(mContext, mDatas) {

    private var emptyId = R.layout.item_empty_layout

    protected abstract fun convert(holder: RViewHolder, t: T, position: Int)

    protected fun getViewLayoutId(viewType: Int): Int {
        return if (viewType == EMPTY_VIEW) { emptyId } else mLayoutId
    }

    fun setEmptyId(emptyId: Int) {
        this.emptyId = emptyId
    }

    init {
        //getItemViewLayoutId重写，则此处layoutId可直接写0
        setItemViewDelegate(object : ItemViewDelegate<T> {
            override fun getItemViewLayoutId(viewType: Int): Int {
                return getViewLayoutId(viewType)
            }

            override fun isForViewType(item: T, position: Int): Boolean {
                return true
            }

            override fun convert(holder: RViewHolder, t: T, position: Int) {
                if (isForViewType(t, position)) {
                    this@RVAdapter.convert(holder, t, position)
                }
            }
        })
    }
}