package com.busybird.store.widget

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.zhou.demo.R


/**
 *
 * @fuction 自定义TextView
 * @date 2019-09-11
 * @author zhouruiyong
 */
class TextViewPlus @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatTextView(context, attrs, defStyleAttr) {

    private var leftHeight = -1
    private var leftWidth = -1
    private var rightHeight = -1
    private var rightWidth = -1
    private var topHeight = -1
    private var topWidth = -1
    private var bottomHeight = -1
    private var bottomWidth = -1

    init {
        init(context, attrs, defStyleAttr)
    }

    /**
     * 初始化读取参数
     */
    private fun init(context: Context, attrs: AttributeSet?, defStyle: Int) {

        // TypeArray中含有我们需要使用的参数
        val a = context.obtainStyledAttributes(
            attrs,
            R.styleable.TextViewPlus, defStyle, 0
        )
        val dl: Drawable? = null
        val dt: Drawable? = null
        val dr: Drawable? = null
        val db: Drawable? = null
        if (a != null) {
            // 获得参数个数
            val count = a.indexCount
            var index = 0
            // 遍历参数。先将index从TypedArray中读出来，
            // 得到的这个index对应于attrs.xml中设置的参数名称在R中编译得到的数
            // 这里会得到各个方向的宽和高
            for (i in 0 until count) {
                index = a.getIndex(i)
                when (index) {
                    R.styleable.TextViewPlus_bottom_height -> bottomHeight = a.getDimensionPixelSize(index, -1)
                    R.styleable.TextViewPlus_bottom_width -> bottomWidth = a.getDimensionPixelSize(index, -1)
                    R.styleable.TextViewPlus_left_height -> leftHeight = a.getDimensionPixelSize(index, -1)
                    R.styleable.TextViewPlus_left_width -> leftWidth = a.getDimensionPixelSize(index, -1)
                    R.styleable.TextViewPlus_right_height -> rightHeight = a.getDimensionPixelSize(index, -1)
                    R.styleable.TextViewPlus_right_width -> rightWidth = a.getDimensionPixelSize(index, -1)
                    R.styleable.TextViewPlus_top_height -> topHeight = a.getDimensionPixelSize(index, -1)
                    R.styleable.TextViewPlus_top_width -> topWidth = a.getDimensionPixelSize(index, -1)
                }
            }

            // 获取各个方向的图片，按照：左-上-右-下 的顺序存于数组中
            val drawables = compoundDrawables
            var dir = 0
            // 0-left; 1-top; 2-right; 3-bottom;
            for (drawable in drawables) {
                // 设定图片大小
                setImageSize(drawable, dir++)
            }
            // 将图片放回到TextView中
            setCompoundDrawables(
                drawables[0], drawables[1], drawables[2],
                drawables[3]
            )

        }

    }


    /**
     * 设定图片的大小
     */
    private fun setImageSize(d: Drawable?, dir: Int) {
        if (d == null) {
            return
        }

        var height = -1
        var width = -1
        // 根据方向给宽和高赋值
        when (dir) {
            0 -> {
                // left
                height = leftHeight
                width = leftWidth
            }
            1 -> {
                // top
                height = topHeight
                width = topWidth
            }
            2 -> {
                // right
                height = rightHeight
                width = rightWidth
            }
            3 -> {
                // bottom
                height = bottomHeight
                width = bottomWidth
            }
        }
        // 如果有某个方向的宽或者高没有设定值，则不去设定图片大小
        if (width != -1 && height != -1) {
            d.setBounds(0, 0, width, height)
        }
    }


    fun setDrawableLeft(res: Int) {
        if (res == 0) {
            setCompoundDrawables(null, null, null, null)
        } else {
            val dleft = ContextCompat.getDrawable(context, res)
            dleft!!.setBounds(0, 0, leftWidth, leftHeight)
            setCompoundDrawables(dleft, null, null, null)
        }

    }

    fun setDrawableTop(res: Int) {
        if (res == 0) {
            setCompoundDrawables(null, null, null, null)
        } else {
            val dTop = ContextCompat.getDrawable(context, res)
            dTop!!.setBounds(0, 0, topWidth, topHeight)
            setCompoundDrawables(null, dTop, null, null)
        }
    }


}
