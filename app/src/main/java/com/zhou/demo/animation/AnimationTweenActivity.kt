package com.zhou.demo.animation

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.zhou.base.listener.NoDoubleClickListener
import com.zhou.demo.R
import kotlinx.android.synthetic.main.activity_animation_tween.*
import kotlinx.android.synthetic.main.item_head_layout.*


/**
 * 帧动画
 */
class AnimationTweenActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
        initListener()
    }




    private fun initUI() {
        setContentView(R.layout.activity_animation_tween)
        tv_title.text = "补间动画"
    }

    private fun initListener() {
        iv_back.setOnClickListener(mNoDoubleClickListener)
        iv_frame.setOnClickListener(mNoDoubleClickListener)
        btn_alpha.setOnClickListener(mNoDoubleClickListener)
        btn_scale.setOnClickListener(mNoDoubleClickListener)
        btn_translate.setOnClickListener(mNoDoubleClickListener)
        btn_rotate.setOnClickListener(mNoDoubleClickListener)
        btn_set.setOnClickListener(mNoDoubleClickListener)
        btn_java.setOnClickListener(mNoDoubleClickListener)
    }

    //防止连续点击
    private val mNoDoubleClickListener: NoDoubleClickListener = object : NoDoubleClickListener() {
        override fun onNoDoubleClick(v: View) {
            when (v.id) {
                R.id.iv_back -> finish()
                R.id.iv_frame -> Toast.makeText(this@AnimationTweenActivity,"点击图片",Toast.LENGTH_SHORT).show()
                R.id.btn_alpha -> {
                    val animation = AnimationUtils.loadAnimation(this@AnimationTweenActivity, R.anim.alpha_anim)
                    iv_frame.startAnimation(animation)
                }
                R.id.btn_scale -> {
                    val animation = AnimationUtils.loadAnimation(this@AnimationTweenActivity, R.anim.scale_anim)
                    iv_frame.startAnimation(animation)
                }
                R.id.btn_translate -> {
                    val animation = AnimationUtils.loadAnimation(this@AnimationTweenActivity, R.anim.translate_anim)
                    iv_frame.startAnimation(animation)
                }
                R.id.btn_rotate -> {
                    val animation = AnimationUtils.loadAnimation(this@AnimationTweenActivity, R.anim.rotate_anim)
                    iv_frame.startAnimation(animation)
                }
                R.id.btn_set-> {
                    val animation = AnimationUtils.loadAnimation(this@AnimationTweenActivity, R.anim.sets_anim)
                    iv_frame.startAnimation(animation)
                }
                R.id.btn_java-> {
                    setRotateAnimation()
                }

            }
        }
    }

    /**
     * 旋转动画
     */
    private fun setRotateAnimation(){
//        效果以图片中心点为中心，从负90度到正90度，持续5s
//        1创建动画对象
        val rotateAnimation = RotateAnimation(0f,360f, Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
//        2设置
        rotateAnimation.duration = 5000
//        设置线性变化的插值器(线性变化，加速变化，减速变化，周期循环变化)
        rotateAnimation.interpolator = LinearInterpolator()
//        动画重复Animation.INFINITE=-1表示无限旋转
        rotateAnimation.repeatCount = Animation.INFINITE
//        3启动动画
        iv_frame.startAnimation(rotateAnimation);
        rotateAnimation.setAnimationListener(object: Animation.AnimationListener{
            override fun onAnimationRepeat(animation: Animation?) {
                Log.v("animation","动画重复")
            }

            override fun onAnimationEnd(animation: Animation?) {
                Log.v("animation","动画结束")
            }

            override fun onAnimationStart(animation: Animation?) {
                Log.v("animation","动画开始")
            }

        })

    }

}