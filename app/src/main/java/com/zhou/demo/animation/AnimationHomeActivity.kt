package com.zhou.demo.animation

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.zhou.base.listener.NoDoubleClickListener
import com.zhou.demo.R
import com.zhou.demo.recyclerview.AnimationActivity
import kotlinx.android.synthetic.main.activity_animation_home.*
import kotlinx.android.synthetic.main.item_head_layout.*

/**
 * 动画
 */
class AnimationHomeActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
        initListener()
    }




    private fun initUI() {
        setContentView(R.layout.activity_animation_home)
        tv_title.text = "动画"
    }

    private fun initListener() {
        iv_back.setOnClickListener(mNoDoubleClickListener)
        animation_btn1.setOnClickListener(mNoDoubleClickListener)
        animation_btn2.setOnClickListener(mNoDoubleClickListener)
        animation_btn3.setOnClickListener(mNoDoubleClickListener)
    }

    //防止连续点击
    private val mNoDoubleClickListener: NoDoubleClickListener = object : NoDoubleClickListener() {
        override fun onNoDoubleClick(v: View) {
            when (v.id) {
                R.id.iv_back -> finish()
                R.id.animation_btn1 -> startActivity(Intent(this@AnimationHomeActivity, AnimationFrameActivity::class.java))
                R.id.animation_btn2 -> startActivity(Intent(this@AnimationHomeActivity, AnimationTweenActivity::class.java))
                R.id.animation_btn3 -> startActivity(Intent(this@AnimationHomeActivity, AnimationObjectActivity::class.java))

            }
        }
    }

}