package com.zhou.demo.animation

import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.zhou.base.listener.NoDoubleClickListener
import com.zhou.demo.R
import kotlinx.android.synthetic.main.activity_animation_frame.*
import kotlinx.android.synthetic.main.activity_animation_home.*
import kotlinx.android.synthetic.main.item_head_layout.*

/**
 * 帧动画
 */
class AnimationFrameActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
        initListener()
        val animationDrawable = iv_frame.drawable as AnimationDrawable
        animationDrawable.start()
    }




    private fun initUI() {
        setContentView(R.layout.activity_animation_frame)
        tv_title.text = "帧动画"
    }

    private fun initListener() {
        iv_back.setOnClickListener(mNoDoubleClickListener)
    }

    //防止连续点击
    private val mNoDoubleClickListener: NoDoubleClickListener = object : NoDoubleClickListener() {
        override fun onNoDoubleClick(v: View) {
            when (v.id) {
                R.id.iv_back -> finish()
            }
        }
    }

}