package com.zhou.demo.animation

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.graphics.PointF
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.appcompat.app.AppCompatActivity
import com.zhou.base.listener.NoDoubleClickListener
import com.zhou.demo.R
import kotlinx.android.synthetic.main.activity_animation_frame.iv_frame
import kotlinx.android.synthetic.main.activity_animation_object.*
import kotlinx.android.synthetic.main.item_head_layout.*


/**
 * 帧动画
 */
class AnimationObjectActivity : AppCompatActivity() {


    private val mCircleColors = arrayOf(0xff00ff00,0xff00ffff,0xffffff00,0xff123456,0xfffff000,0xff129456)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
        initListener()
    }




    private fun initUI() {
        setContentView(R.layout.activity_animation_object)
        tv_title.text = "属性动画"
    }

    private fun initListener() {
        iv_back.setOnClickListener(mNoDoubleClickListener)
        btn1.setOnClickListener(mNoDoubleClickListener)
        btn2.setOnClickListener(mNoDoubleClickListener)
        btn3.setOnClickListener(mNoDoubleClickListener)
    }

    //防止连续点击
    private val mNoDoubleClickListener: NoDoubleClickListener = object : NoDoubleClickListener() {
        override fun onNoDoubleClick(v: View) {
            when (v.id) {
                R.id.iv_back -> finish()
                R.id.btn1 -> {
                    javaDoing() //java实现
//                    xmlDoing() //xml实现
                }
                R.id.btn2 -> startEvaluator()
                R.id.btn3 -> {
                    //普通
//                    val animator: ObjectAnimator = ObjectAnimator.ofFloat(iv_frame, "rotation", 0f, 180f, 0f)
//                    animator.duration = 1000
//                    animator.start()

                    val moveIn = ObjectAnimator.ofFloat(iv_frame, "translationX", -500f, 0f)
                    val rotate = ObjectAnimator.ofFloat(iv_frame, "rotation", 0f, 360f)
                    val fadeInOut = ObjectAnimator.ofFloat(iv_frame, "alpha", 1f, 0f, 1f)
                    val animSet = AnimatorSet()
                    animSet.play(rotate).with(fadeInOut).after(moveIn)
                    animSet.duration = 5000
                    animSet.start()
                }
            }
        }
    }

    fun javaDoing(){
        //设置参数：开始值0和结束值360，表示你要从哪个值到哪个值进行变化（旋转角度）
        val valueAnimator = ValueAnimator.ofFloat(0f, 360f)
        //这里是设置动画执行时间，毫秒
        valueAnimator.duration = 2000
        //设置动画的执行次数，-1表示循环执行,不然就是执行n+1次
        valueAnimator.repeatCount = 0
        val interpolator = AccelerateDecelerateInterpolator()
        valueAnimator.interpolator = interpolator
        //动画的下次执行开始位置，RESTART表示动画每次从原始的状态执行，REVERSE表示动画第二次执行要从第一次改变后的状态逆向执行
        valueAnimator.repeatMode = ValueAnimator.RESTART
        //设置数值变化监听器，动画变化时的值会在这个监听器中返回给我们，我们来手动赋值，来进行动画显示
        valueAnimator.addUpdateListener { animation -> //获取当前变化时的值
            val value = animation.animatedValue as Float
            Log.e("valueAnimator", "value:$value")
            //这里我们给textview设置一个文本，就是我们的当前的数值，那么就会显示为从0到100一直变化的动画
            iv_frame.pivotX = 0f
            iv_frame.pivotY = 0f //设置旋转中心店
            iv_frame.rotation = value
        }
        //开始执行动画
        valueAnimator.start()
    }

    fun xmlDoing(){
        val animator = AnimatorInflater.loadAnimator(this, R.animator.property_rotate) as ValueAnimator
        animator.addUpdateListener { animation -> //获取当前变化时的值
            val value = animation.animatedValue as Float
            Log.e("valueAnimator", "value:$value")
            //这里我们给textview设置一个文本，就是我们的当前的数值，那么就会显示为从0到100一直变化的动画
            iv_frame.pivotX = 0f
            iv_frame.pivotY = 0f //设置旋转中心店
            iv_frame.rotation = value
        }
        animator.setTarget(iv_frame)
        animator.start()
    }


    /**
     * 使用估值器实现重力下落
     *
     * @param v
     */
    fun startEvaluator() {
        val animator = ValueAnimator()
        animator.duration = 3000
        animator.setObjectValues(PointF(0f, 0f))
        val pointF = PointF()
        animator.setEvaluator { fraction, startValue, endValue -> //fraction是运动中的匀速变化的值
            //根据重力计算实际的运动y=vt=0.5*g*t*t
            //g越大效果越明显
            pointF.x = 100 * (fraction * 5)
            pointF.y = 0.5f * 300f * (fraction * 5) * (fraction * 5)
            pointF
        }
        animator.addUpdateListener { animation ->
            val p = animation.animatedValue as PointF
            iv_frame.x = p.x
            iv_frame.y = p.y
        }
        animator.start()
    }
    
}