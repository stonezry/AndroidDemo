package com.zhou.demo.recyclerview

import android.graphics.Canvas
import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.zhou.base.listener.NoDoubleClickListener
import com.zhou.demo.R
import com.zhou.demo.recyclerview.LinearManagerActivity.HomeAdapter.HomeViewHolder
import com.zhou.demo.utils.DensityUtils
import kotlinx.android.synthetic.main.activity_rv_linear.*
import kotlinx.android.synthetic.main.item_head_layout.*


class LinearManagerActivity : AppCompatActivity() {

    private var homeAdapter: HomeAdapter? = null
    private val mDatas = ArrayList<String>()
    private var isFirst = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
        initListener()
        isFirst = true
    }

    private fun initUI() {
        setContentView(R.layout.activity_rv_linear)
        tv_title.setText(R.string.linearmanager)
        val linearLayoutManager = LinearLayoutManager(this)
//        linearLayoutManager.setReverseLayout(true); //反转
//        linearLayoutManager.setStackFromEnd(true);
//        linearLayoutManager.scrollToPosition(3);
//        linearLayoutManager.scrollToPositionWithOffset(3, 30)
        rv_linear.layoutManager = linearLayoutManager
        rv_linear.addItemDecoration(MyDecoration())
        homeAdapter = HomeAdapter()
        rv_linear.adapter = homeAdapter
    }

    private fun initListener() {
        iv_back!!.setOnClickListener(mNoDoubleClickListener)
    }



    override fun onResume() {
        super.onResume()
        if (isFirst) {
            isFirst = false
            downJson()
        }
    }

    private fun downJson() {
        mDatas.clear()
        for (i in 0..19) {
            mDatas.add("RecyclerView使用$i")
        }
        homeAdapter!!.notifyDataSetChanged()
    }


    //防止连续点击
    private val mNoDoubleClickListener: NoDoubleClickListener = object : NoDoubleClickListener() {

        override fun onNoDoubleClick(v: View) {
            when (v.id) {
                R.id.iv_back -> finish()
            }
        }

    }

    internal inner class HomeAdapter : RecyclerView.Adapter<HomeViewHolder?>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
            val itemView: View = LayoutInflater.from(this@LinearManagerActivity)
                .inflate(R.layout.item_rv_txt, parent, false)
            return HomeViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
            holder.tv.text = mDatas[position]
        }


        internal inner class HomeViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv: TextView  = view.findViewById(R.id.txt)
        }

        override fun getItemCount(): Int {
            return mDatas.size
        }


    }

    /**
     * 间隔
     */
    internal inner class MyDecoration : RecyclerView.ItemDecoration() {
        override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
            super.onDraw(c, parent, state)
        }

        override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
            super.onDrawOver(c, parent, state)
        }

        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            super.getItemOffsets(outRect, view, parent, state)
            outRect.bottom = DensityUtils.dp_px(this@LinearManagerActivity, 10f)
        }
    }

}