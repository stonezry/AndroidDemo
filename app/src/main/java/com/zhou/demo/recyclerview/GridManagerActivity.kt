package com.zhou.demo.recyclerview

import android.graphics.Canvas
import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zhou.base.listener.NoDoubleClickListener
import com.zhou.demo.R
import com.zhou.demo.utils.DensityUtils
import kotlinx.android.synthetic.main.activity_rv_grid.*
import kotlinx.android.synthetic.main.item_head_layout.*
import java.util.*

class GridManagerActivity : AppCompatActivity() {

    private var homeAdapter: HomeAdapter? = null
    private val mDatas = ArrayList<String>()
    private var isFirst = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
        initListener()
        isFirst = true
    }

    override fun onResume() {
        super.onResume()
        if (isFirst) {
            isFirst = false
            downJson()
        }
    }

    private fun downJson() {
        mDatas.clear()
        for (i in 0..19) {
            mDatas.add("RecyclerView使用$i")
        }
        homeAdapter!!.notifyDataSetChanged()
    }

    private fun initUI() {
        setContentView(R.layout.activity_rv_grid)
        tv_title.setText(R.string.gridmanager)
        val gridLayoutManager = GridLayoutManager(this, 3)
        recyclerview.layoutManager = gridLayoutManager
        recyclerview.addItemDecoration(MyDecoration())
        homeAdapter = HomeAdapter()
        recyclerview.adapter = homeAdapter
    }

    private fun initListener() {
        iv_back!!.setOnClickListener(mNoDoubleClickListener)
    }

    //防止连续点击
    private val mNoDoubleClickListener: NoDoubleClickListener = object : NoDoubleClickListener() {
        override fun onNoDoubleClick(v: View) {
            when (v.id) {
                R.id.iv_back -> finish()
            }
        }
    }

    internal inner class HomeAdapter : RecyclerView.Adapter<HomeAdapter.HomeViewHolder?>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
            val itemView: View = LayoutInflater.from(this@GridManagerActivity)
                .inflate(R.layout.item_rv_txt, parent, false)
            return HomeViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
            holder.tv.text = mDatas[position]
        }


        internal inner class HomeViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv: TextView  = view.findViewById(R.id.txt)
        }

        override fun getItemCount(): Int {
            return mDatas.size
        }


    }

    /**
     * 间隔
     */
    internal inner class MyDecoration : RecyclerView.ItemDecoration() {
        override fun onDraw(c: Canvas,parent: RecyclerView,state: RecyclerView.State) {
            super.onDraw(c, parent, state)
        }

        override fun onDrawOver(c: Canvas,parent: RecyclerView,state: RecyclerView.State) {
            super.onDrawOver(c, parent, state)
        }

        override fun getItemOffsets(outRect: Rect,view: View,parent: RecyclerView,state: RecyclerView.State) {
            super.getItemOffsets(outRect, view, parent, state)
            val space: Int = DensityUtils.dp_px(this@GridManagerActivity, 10f)
            val pos: Int = parent.getChildAdapterPosition(view)
            when (pos % 3) {
                0 -> {
                    outRect.left = space
                    outRect.right = space / 2
                }
                1 -> {
                    outRect.left = space / 2
                    outRect.right = space / 2
                }
                2 -> {
                    outRect.left = space / 2
                    outRect.right = space
                }
            }
            outRect.bottom = space
        }
    }
}