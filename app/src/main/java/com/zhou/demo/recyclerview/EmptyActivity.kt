package com.zhou.demo.recyclerview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zhou.base.listener.NoDoubleClickListener
import com.zhou.demo.R
import kotlinx.android.synthetic.main.activity_rv_empty.*
import kotlinx.android.synthetic.main.item_head_layout.*
import java.util.*
import kotlin.collections.ArrayList

class EmptyActivity : AppCompatActivity() {

    private var homeAdapter: HomeAdapter? = null
    private val mDatas: ArrayList<String> = ArrayList()
    private var isFirst = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
        initListener()
        isFirst = true
    }

    override fun onResume() {
        super.onResume()
        if (isFirst) {
            isFirst = false
            downJson()
        }
    }

    private fun downJson() {
        mDatas.clear()
        homeAdapter!!.notifyDataSetChanged()
    }

    //   StaggeredGridLayoutManager layoutManager;
    //     LinearLayoutManager layoutManager;
    var layoutManager: GridLayoutManager? = null


    private fun initUI() {
        setContentView(R.layout.activity_rv_empty)
        tv_title.setText(R.string.empty_view)

//       LinearLayoutManager layoutManager = new LinearLayoutManager(this); //线性布局
        layoutManager = GridLayoutManager(this, 2) //表格布局

        //设置表格，根据position计算在该position处item的跨度(占几列数据)
        layoutManager!!.setSpanSizeLookup(object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                //计算在哪个position更换跨度，要占满一行跨度就是2
                return if (mDatas.size > 0) {
                    1
                } else {
                    2
                }
            }
        })

//        layoutManager = new StaggeredGridLayoutManager(2,RecyclerView.VERTICAL); //普遍布局布局
        recyclerview.layoutManager = layoutManager
        homeAdapter = HomeAdapter()
        recyclerview.adapter = homeAdapter
    }

    private fun initListener() {
        iv_back.setOnClickListener(mNoDoubleClickListener)
        btn_clear.setOnClickListener(mNoDoubleClickListener)
        btn_generate.setOnClickListener(mNoDoubleClickListener)
    }

    //防止连续点击
    private val mNoDoubleClickListener: NoDoubleClickListener = object : NoDoubleClickListener() {
        override fun onNoDoubleClick(v: View) {
            when (v.id) {
                R.id.iv_back -> finish()
                R.id.btn_clear -> {
                    mDatas.clear()
                    //                    layoutManager.setSpanCount(1);
                    homeAdapter!!.notifyDataSetChanged()
                }
                R.id.btn_generate -> {
                    mDatas.clear()
                    val x = Random().nextInt(20)
                    var i = 0
                    while (i < x) {
                        mDatas.add("RecyclerView使用$i")
                        i++
                    }
//                    if (mDatas.size() == 0){
//                        layoutManager.setSpanCount(1);
//                    }else{
//                        layoutManager.setSpanCount(2);
//                    }
                    homeAdapter!!.notifyDataSetChanged()
                }
            }
        }
    }

    internal inner class HomeAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {
        private val EMPTY_VIEW = 0
        private val NOT_EMPTY_VIEW = 1
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            if (viewType == EMPTY_VIEW) {
                return EmptyViewHolder(LayoutInflater.from(this@EmptyActivity).inflate(R.layout.item_empty_layout, parent, false))
            }
            val itemView: View = LayoutInflater.from(this@EmptyActivity).inflate(R.layout.item_rv_txt, parent, false)
            return HomeViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            if (getItemViewType(position) == EMPTY_VIEW) {
                return
            }
            val homeViewHolder =
                holder as HomeViewHolder
            homeViewHolder.tv.text = mDatas!![position]
        }

        override fun getItemCount(): Int {
            return if (mDatas.size > 0) mDatas.size else 1
        }

        override fun getItemViewType(position: Int): Int {
            //根据传入adapter来判断是否有数据
            return if (mDatas.size > 0) NOT_EMPTY_VIEW else EMPTY_VIEW
        }

        internal inner class HomeViewHolder(view: View) :
            RecyclerView.ViewHolder(view) {
            var tv: TextView = view.findViewById(R.id.txt)

        }

        internal inner class EmptyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    }
}