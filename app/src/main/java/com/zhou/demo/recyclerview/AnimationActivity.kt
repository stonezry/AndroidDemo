package com.zhou.demo.recyclerview

import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zhou.base.listener.NoDoubleClickListener
import com.zhou.demo.R
import kotlinx.android.synthetic.main.activity_rv_animation.*
import kotlinx.android.synthetic.main.item_head_layout.*


class AnimationActivity : AppCompatActivity() {

    private var homeAdapter: HomeAdapter? = null
    private val mDatas = ArrayList<String>()
    private var isFirst = false

    private val mHandler: Handler = object: Handler(){
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            if(mDatas.size > 2){
                val mes = mDatas.removeAt(0)
                homeAdapter!!.notifyItemRemoved(0)

                mDatas.add(mes)
                homeAdapter!!.notifyItemInserted(mDatas.size-1)
                sendEmptyMessageDelayed(1,2500)
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
        initListener()
        isFirst = true
    }

    private fun initUI() {
        setContentView(R.layout.activity_rv_animation)
        tv_title.text = "上下滑动的动画效果"
        val linearLayoutManager = LinearLayoutManager(this)
        rv_message.layoutManager = linearLayoutManager
        homeAdapter = HomeAdapter()
        rv_message.adapter = homeAdapter

        /**
         * 既然是动画，就会有时间，我们把动画执行时间变大一点来看一看效果
         */
        val defaultItemAnimator = DefaultItemAnimator()
        defaultItemAnimator.addDuration = 500
        defaultItemAnimator.removeDuration = 500
        rv_message.itemAnimator = defaultItemAnimator
    }

    private fun initListener() {
        iv_back.setOnClickListener(mNoDoubleClickListener)
    }



    override fun onResume() {
        super.onResume()
        if (isFirst) {
            isFirst = false
            downJson()
        }
    }

    private fun downJson() {
        mDatas.clear()
        for (i in 0..10) {
            mDatas.add("RecyclerView使用$i")
        }
        homeAdapter!!.notifyDataSetChanged()

        mHandler.sendEmptyMessageDelayed(1,2500)
    }


    //防止连续点击
    private val mNoDoubleClickListener: NoDoubleClickListener = object : NoDoubleClickListener() {

        override fun onNoDoubleClick(v: View) {
            when (v.id) {
                R.id.iv_back -> finish()
            }
        }

    }

    internal inner class HomeAdapter : RecyclerView.Adapter<HomeAdapter.HomeViewHolder?>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
            val itemView: View = LayoutInflater.from(this@AnimationActivity)
                .inflate(R.layout.item_rv_message, parent, false)
            return HomeViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
            holder.tv_content.text = mDatas[position]
        }


        internal inner class HomeViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_content: TextView  = view.findViewById(R.id.tv_content)
        }

        override fun getItemCount(): Int {
            return if(mDatas.size > 2) 2 else mDatas.size
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        mHandler.removeCallbacksAndMessages(null)
    }



}