package com.zhou.demo.recyclerview

import android.graphics.Canvas
import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.zhou.base.listener.NoDoubleClickListener
import com.zhou.base.listener.OnItemClickListener
import com.zhou.demo.R
import com.zhou.demo.utils.DensityUtils
import kotlinx.android.synthetic.main.activity_rv_staggered.*
import kotlinx.android.synthetic.main.item_head_layout.*

class StaggeredManagerActivity : AppCompatActivity() {
    private var homeAdapter: HomeAdapter? = null
    private val mDatas = ArrayList<String>()
    private var isFirst = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
        initListener()
        isFirst = true
    }

    override fun onResume() {
        super.onResume()
        if (isFirst) {
            isFirst = false
            downJson()
        }
    }

    private fun downJson() {
        mDatas.clear()
        val res = "你好你好"
        for (i in 0..49) {
            val num = (Math.random() * 20 + 1).toInt()
            val stringBuilder = StringBuilder()
            for (j in 0 until num) {
                stringBuilder.append(res, 0, res.length)
            }
            mDatas.add(stringBuilder.toString())
            stringBuilder.delete(0, stringBuilder.length)
        }
        homeAdapter!!.notifyDataSetChanged()
    }

    private fun initUI() {
        setContentView(R.layout.activity_rv_staggered)
        tv_title.setText(R.string.staggeredmanager)
        //瀑布流布局管理器
        val manager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        recyclerview.layoutManager = manager
        recyclerview.addItemDecoration(MyDecoration())
        homeAdapter = HomeAdapter()
        recyclerview.adapter = homeAdapter
        homeAdapter!!.setOnItemClickListener(object : OnItemClickListener {
            override fun onItemClick(view: View?, holder: RecyclerView.ViewHolder?, position: Int) {
                Toast.makeText(this@StaggeredManagerActivity, "点击第" + position + "个", Toast.LENGTH_SHORT).show()
            }

            override fun onItemLongClick(view: View?, holder: RecyclerView.ViewHolder?, position: Int): Boolean {
                Toast.makeText(this@StaggeredManagerActivity, "长按第" + position + "个", Toast.LENGTH_SHORT).show()
                return true
            }
        })
    }

    private fun initListener() {
        iv_back!!.setOnClickListener(mNoDoubleClickListener)
    }

    //防止连续点击
    private val mNoDoubleClickListener: NoDoubleClickListener = object : NoDoubleClickListener() {
        override fun onNoDoubleClick(v: View) {
            when (v.id) {
                R.id.iv_back -> finish()
            }
        }
    }

    internal inner class HomeAdapter : RecyclerView.Adapter<HomeAdapter.HomeViewHolder?>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {

            val itemView: View = LayoutInflater.from(this@StaggeredManagerActivity).inflate(R.layout.item_rv_txt, parent, false)
            val holder = HomeViewHolder(itemView)
            itemView.setOnClickListener { v ->
                mOnItemClickListener?.onItemClick(v, holder, holder.adapterPosition)
            }

            itemView.setOnLongClickListener(View.OnLongClickListener { v ->
                if (mOnItemClickListener != null) {
                    return@OnLongClickListener mOnItemClickListener!!.onItemLongClick(v, holder, holder.adapterPosition)
                }
                false
            })
            return holder
        }

        override fun onBindViewHolder(
            holder: HomeViewHolder,
            position: Int
        ) {
            holder.tv.text = mDatas[position]
        }

        override fun getItemCount(): Int {
            return mDatas.size
        }

        internal inner class HomeViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv: TextView = view.findViewById(R.id.txt)
        }

        var mOnItemClickListener: OnItemClickListener? = null
        fun setOnItemClickListener(onItemClickListener: OnItemClickListener?) {
            mOnItemClickListener = onItemClickListener
        }
    }

    /**
     * 间隔
     */
    internal inner class MyDecoration : RecyclerView.ItemDecoration() {
        override fun onDraw(c: Canvas,parent: RecyclerView,state: RecyclerView.State
        ) {
            super.onDraw(c, parent, state)
        }

        override fun onDrawOver(c: Canvas,parent: RecyclerView,state: RecyclerView.State) {
            super.onDrawOver(c, parent, state)
        }

        override fun getItemOffsets(outRect: Rect,view: View,parent: RecyclerView,state: RecyclerView.State) {
            super.getItemOffsets(outRect, view, parent, state)
            outRect.bottom = DensityUtils.dp_px(this@StaggeredManagerActivity, 1f)
            outRect.left = DensityUtils.dp_px(this@StaggeredManagerActivity, 1f)
        }
    }
}