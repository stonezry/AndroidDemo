package com.zhou.demo.recyclerview

import android.content.Context
import android.content.Intent
import android.graphics.*
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zhou.base.listener.NoDoubleClickListener
import com.zhou.base.listener.OnItemClickListener
import com.zhou.demo.R
import com.zhou.demo.utils.DensityUtils
import kotlinx.android.synthetic.main.activity_rv_index.*
import kotlinx.android.synthetic.main.item_head_layout.*

class RecyclerviewActivity : AppCompatActivity() {

    private var homeAdapter: HomeAdapter? = null
    private val mDatas = ArrayList<String>()
    private var isFirst = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
        initListener()
        isFirst = true
    }


    private fun initUI() {
        setContentView(R.layout.activity_rv_index)
        tv_title.text = "Recyclerview"
        recyclerview.layoutManager = LinearLayoutManager(this)
        recyclerview.addItemDecoration(MyItemDecoration(this, RecyclerView.VERTICAL))
        recyclerview.itemAnimator = DefaultItemAnimator()
        homeAdapter = HomeAdapter()
        recyclerview.adapter = homeAdapter
        homeAdapter!!.setOnItemClickListener(object : OnItemClickListener {
            override fun onItemClick(view: View?, holder: RecyclerView.ViewHolder?, position: Int) {
                when (position) {
                    0 -> startActivity(Intent(this@RecyclerviewActivity, LinearManagerActivity::class.java))
                    1 -> startActivity(Intent(this@RecyclerviewActivity, GridManagerActivity::class.java))
                    2 -> startActivity(Intent(this@RecyclerviewActivity, StaggeredManagerActivity::class.java))
                    3 -> {
                        startActivity(Intent(this@RecyclerviewActivity, EmptyActivity::class.java))
                    }
                    4 -> {
                        startActivity(Intent(this@RecyclerviewActivity, AnimationActivity::class.java))
                    }
                    else -> Toast.makeText(
                        this@RecyclerviewActivity,
                        "点击第" + position + "个",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onItemLongClick(
                view: View?,
                holder: RecyclerView.ViewHolder?,
                position: Int
            ): Boolean {
                Toast.makeText(this@RecyclerviewActivity, "长按第" + position + "个", Toast.LENGTH_SHORT).show()
                return true
            }
        })
    }

    private fun initListener() {
        iv_back.setOnClickListener(mNoDoubleClickListener)
    }

    override fun onResume() {
        super.onResume()
        if (isFirst) {
            isFirst = false
            downJson()
        }
    }

    private fun downJson() {
        mDatas.clear()
        for (i in 0..19) {
            if (i == 0) {
                mDatas.add("线性LinearLayoutManager")
            } else if (i == 1) {
                mDatas.add("表格GridLayoutManager")
            } else if (i == 2) {
                mDatas.add("瀑布流StaggeredGridLayoutManager")
            } else if (i == 3) {
                mDatas.add("Set emptyView")
            } else if (i == 4) {
                mDatas.add("上下循环循环滑动的效果")
            }  else {
                mDatas.add("RecyclerView使用$i")
            }
        }
        homeAdapter!!.notifyDataSetChanged()
    }


    //防止连续点击
    private val mNoDoubleClickListener: NoDoubleClickListener = object : NoDoubleClickListener() {
        override fun onNoDoubleClick(v: View) {
            when (v.id) {
                R.id.iv_back -> finish()
            }
        }
    }

    internal inner class HomeAdapter : RecyclerView.Adapter<HomeAdapter.HomeViewHolder?>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
            val itemView: View = LayoutInflater.from(this@RecyclerviewActivity)
                .inflate(R.layout.item_rv_txt, parent, false)

            val holder = HomeViewHolder(itemView)
            itemView.setOnClickListener { v ->
                mOnItemClickListener?.onItemClick(v, holder, holder.adapterPosition)
            }
            itemView.setOnLongClickListener(View.OnLongClickListener { v ->
                if (mOnItemClickListener != null) {
                    return@OnLongClickListener mOnItemClickListener!!.onItemLongClick(v, holder, holder.adapterPosition)
                }
                false
            })

            return holder
        }

        override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
            holder.tv.text = mDatas[position]
        }


        internal inner class HomeViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv: TextView  = view.findViewById(R.id.txt)
        }

        override fun getItemCount(): Int {
            return mDatas.size
        }

        var mOnItemClickListener: OnItemClickListener? = null
        fun setOnItemClickListener(onItemClickListener: OnItemClickListener?) {
            mOnItemClickListener = onItemClickListener
        }

    }
    /**
     * 间隔
     */
    internal inner class MyDecoration : RecyclerView.ItemDecoration() {
        override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
            super.onDraw(c, parent, state)
        }

        override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
            super.onDrawOver(c, parent, state)
        }

        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            super.getItemOffsets(outRect, view, parent, state)
            outRect.bottom = DensityUtils.dp_px(this@RecyclerviewActivity, 10f)
        }
    }

    /**
     * 绘制间隔
     */
    internal inner class MyItemDecoration(
        context: Context,
        private val orientation: Int
    ) : RecyclerView.ItemDecoration() {

        private val mLine: Drawable?
        private val bitmap: Bitmap
        private val mPaint: Paint

        override fun onDraw(
            c: Canvas,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            super.onDraw(c, parent, state)
            if (orientation == RecyclerView.HORIZONTAL) {
                drawVertical(c, parent, state)
            } else if (orientation == RecyclerView.VERTICAL) {
                drawHorizontal(c, parent, state)
            }
        }

        override fun onDrawOver(
            c: Canvas,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            super.onDrawOver(c, parent, state)
            val childCount: Int = parent.getChildCount()
            for (i in 0 until childCount) {
                val child: View = parent.getChildAt(i)
                val left = child.left
                val top = child.top
                c.drawBitmap(bitmap, left.toFloat(), top.toFloat(), mPaint)
            }
        }

        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            super.getItemOffsets(outRect, view, parent, state)
            if (orientation == RecyclerView.HORIZONTAL) {
                //画垂直线
                outRect[0, 0, mLine!!.intrinsicWidth] = 0
            } else if (orientation == RecyclerView.VERTICAL) {
                //画水平线
                outRect[0, 0, 0] = mLine!!.intrinsicHeight
            }
        }

        /**
         * 画垂直分割线
         */
        private fun drawVertical(
            c: Canvas,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            val childCount: Int = parent.getChildCount()
            for (i in 0 until childCount) {
                val child: View = parent.getChildAt(i)
                val left = child.right
                val top = child.top
                val right = left + mLine!!.intrinsicWidth
                val bottom = child.bottom
                mLine.setBounds(left, top, right, bottom)
                mLine.draw(c)
            }
        }

        /**
         * 画水平分割线
         */
        private fun drawHorizontal(
            c: Canvas,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            val childCount: Int = parent.getChildCount()
            for (i in 0 until childCount) {
                val child: View = parent.getChildAt(i)
                val left = child.left
                val top = child.bottom
                val right = child.right
                val bottom = top + mLine!!.intrinsicHeight
                mLine.setBounds(left, top, right, bottom)
                mLine.draw(c)
            }
        }

        init {
            val attrs = intArrayOf(android.R.attr.listDivider)
            val a = context.obtainStyledAttributes(attrs)
            mLine = a.getDrawable(0)
            a.recycle()
            mPaint = Paint()
            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher)
        }
    }

}
