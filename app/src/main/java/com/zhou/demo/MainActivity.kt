package com.zhou.demo

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Adapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.zhou.demo.adapter.AdapterActivity
import com.zhou.demo.adapter.AdapterHomeActivity
import com.zhou.demo.animation.AnimationHomeActivity
import com.zhou.demo.recyclerview.AnimationActivity
import com.zhou.demo.recyclerview.RecyclerviewActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(),View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
        initListener()
        test()
    }

    private fun initUI(){
        setContentView(R.layout.activity_main)
    }

    private fun initListener(){
        recyclerview.setOnClickListener(this)
        map_btn.setOnClickListener(this)
        adapter_btn.setOnClickListener(this)
        animation_btn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        v?.id?.also {
            when(it){
                R.id.recyclerview -> startActivity(Intent(this@MainActivity,RecyclerviewActivity::class.java))
                R.id.map_btn -> {

                    val uri=Uri.parse("geo:24.473306,118.123456");  //打开地图定位
                    val it = Intent(Intent.ACTION_VIEW, uri)

                    val cn = it.resolveActivity(packageManager)

                    if(cn == null){
                        Toast.makeText(this@MainActivity,"请先安装第三方导航软件",Toast.LENGTH_SHORT).show()
                    }else{
                        Log.v("MainActivity",cn.packageName)
                        startActivity(it)
                    }
                }
                R.id.adapter_btn -> startActivity(Intent(this@MainActivity,AdapterHomeActivity::class.java))
                R.id.animation_btn -> startActivity(Intent(this@MainActivity, AnimationHomeActivity::class.java))
            }
        }
    }

    fun test(){

    }


}
