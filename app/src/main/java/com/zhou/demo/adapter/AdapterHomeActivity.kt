package com.zhou.demo.adapter

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zhou.base.adapter.MultiItemTypeAdapter
import com.zhou.base.adapter.RViewHolder
import com.zhou.base.listener.NoDoubleClickListener
import com.zhou.demo.R
import kotlinx.android.synthetic.main.activity_adapter_home.*
import kotlinx.android.synthetic.main.activity_rv_empty.*
import kotlinx.android.synthetic.main.item_head_layout.*
import java.util.*
import kotlin.collections.ArrayList
import com.zhou.base.adapter.RVAdapter as RVAdapter

class AdapterHomeActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
        initListener()
    }




    private fun initUI() {
        setContentView(R.layout.activity_adapter_home)
        tv_title.text = "adapter home"
    }

    private fun initListener() {
        iv_back.setOnClickListener(mNoDoubleClickListener)
        adapter_btn1.setOnClickListener(mNoDoubleClickListener)
        adapter_btn2.setOnClickListener(mNoDoubleClickListener)
        adapter_btn3.setOnClickListener(mNoDoubleClickListener)
        adapter_btn4.setOnClickListener(mNoDoubleClickListener)
    }

    //防止连续点击
    private val mNoDoubleClickListener: NoDoubleClickListener = object : NoDoubleClickListener() {
        override fun onNoDoubleClick(v: View) {
            when (v.id) {
                R.id.iv_back -> finish()
                R.id.adapter_btn1 -> startActivity(Intent(this@AdapterHomeActivity,AdapterActivity::class.java))
                R.id.adapter_btn2 -> startActivity(Intent(this@AdapterHomeActivity,AdapterMoreLoad1Activity::class.java))
                R.id.adapter_btn3 -> startActivity(Intent(this@AdapterHomeActivity,AdapterMoreLoad2Activity::class.java))
                R.id.adapter_btn4 -> startActivity(Intent(this@AdapterHomeActivity,AdapterMoreLoad3Activity::class.java))
            }
        }
    }

}