package com.zhou.demo.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zhou.base.adapter.MultiItemTypeAdapter
import com.zhou.base.adapter.RViewHolder
import com.zhou.base.listener.NoDoubleClickListener
import com.zhou.demo.R
import kotlinx.android.synthetic.main.activity_rv_empty.*
import kotlinx.android.synthetic.main.item_head_layout.*
import java.util.*
import kotlin.collections.ArrayList
import com.zhou.base.adapter.RVAdapter as RVAdapter

class AdapterActivity : AppCompatActivity() {

    private var homeAdapter: RVAdapter<String>? = null
    private val mDatas: ArrayList<String> = ArrayList()
    private var isFirst = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
        initListener()
        isFirst = true
    }

    override fun onResume() {
        super.onResume()
        if (isFirst) {
            isFirst = false
            downJson()
        }
    }

    private fun downJson() {
        mDatas.clear()
        for (i in 1..20){
            mDatas.add("RecyclerView使用$i")
        }
        homeAdapter!!.notifyDataSetChanged()
    }


    private fun initUI() {
        setContentView(R.layout.activity_adapter)
        tv_title.text = "通用的adapter"

        val layoutManager = LinearLayoutManager(this); //线性布局
        recyclerview.layoutManager = layoutManager
        homeAdapter = object: RVAdapter<String>(this@AdapterActivity,R.layout.item_rv_txt, mDatas){
            override fun convert(holder: RViewHolder, t: String, position: Int) {
                holder.setText(R.id.txt,mDatas[position])
            }


        }

        recyclerview.adapter = homeAdapter

        homeAdapter!!.setOnItemClickListener(object: MultiItemTypeAdapter.OnItemClickListener{
            override fun onItemClick(view: View, holder: RecyclerView.ViewHolder, position: Int) {
                TODO("Not yet implemented")
            }

            override fun onItemLongClick(
                view: View,
                holder: RecyclerView.ViewHolder,
                position: Int
            ): Boolean {
                TODO("Not yet implemented")
            }

        })
    }

    private fun initListener() {
        iv_back.setOnClickListener(mNoDoubleClickListener)
    }

    //防止连续点击
    private val mNoDoubleClickListener: NoDoubleClickListener = object : NoDoubleClickListener() {
        override fun onNoDoubleClick(v: View) {
            when (v.id) {
                R.id.iv_back -> finish()
            }
        }
    }

}