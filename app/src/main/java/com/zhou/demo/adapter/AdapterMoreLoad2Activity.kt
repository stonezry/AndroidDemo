package com.zhou.demo.adapter

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zhou.base.adapter.MultiItemTypeAdapter
import com.zhou.base.adapter.RVLoadMoreAdapter
import com.zhou.base.adapter.RViewHolder
import com.zhou.base.listener.NoDoubleClickListener
import com.zhou.demo.R
import kotlinx.android.synthetic.main.activity_adapter_more.*
import kotlinx.android.synthetic.main.activity_rv_empty.*
import kotlinx.android.synthetic.main.activity_rv_empty.recyclerview
import kotlinx.android.synthetic.main.item_head_layout.*
import java.util.*
import kotlin.collections.ArrayList
import com.zhou.base.adapter.RVAdapter as RVAdapter

class AdapterMoreLoad2Activity : AppCompatActivity() {

    private var homeAdapter: RVLoadMoreAdapter<String>? = null
    private val mDatas: ArrayList<String> = ArrayList()
    private var isFirst = false
    private var mCurrentPage: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
        initListener()
        isFirst = true
    }

    override fun onResume() {
        super.onResume()
        if (isFirst) {
            isFirst = false
            downJson(1)
        }
    }

    private fun downJson(page: Int) {

        Handler().postDelayed(Runnable {
            swipe_layout.isRefreshing = false
            if(page == 1){
                mDatas.clear()
                homeAdapter!!.setLoadMoreComplete(true)
            }
            mCurrentPage = page
            val datalist = ArrayList<String>()

            if(page == 3){ //模拟滑动到最底部
                for (i in 1..7){
                    datalist.add("RecyclerView使用$i")
                }
            }else{
                for (i in 1..20){
                    datalist.add("RecyclerView使用$i")
                }
            }
            mDatas.addAll(datalist)
            if (datalist.size < 20) {
                homeAdapter!!.setLoadMoreComplete(false)
            }

            homeAdapter!!.notifyDataSetChanged()
            homeAdapter!!.Loading(false)
        },1000)

    }


    private fun initUI() {
        setContentView(R.layout.activity_adapter_more)
        tv_title.text = "加载更多（grid）"

        swipe_layout.isRefreshing = true
        val layoutManager = GridLayoutManager(this,2); //线性布局
        recyclerview.layoutManager = layoutManager
        homeAdapter = object: RVLoadMoreAdapter<String>(this@AdapterMoreLoad2Activity,recyclerview,R.layout.item_rv_txt, mDatas){
            override fun convert(holder: RViewHolder, t: String, position: Int) {
                holder.setText(R.id.txt,mDatas[position])
            }

        }

        recyclerview.adapter = homeAdapter

        homeAdapter!!.setOnItemClickListener(object: RVLoadMoreAdapter.OnItemClickListener{
            override fun onItemClick(view: View, holder: RecyclerView.ViewHolder, position: Int) {

            }

            override fun onItemLongClick(
                view: View,
                holder: RecyclerView.ViewHolder,
                position: Int
            ): Boolean {
                return false
            }

        })
        layoutManager.spanSizeLookup = object: GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (homeAdapter!!.getItemViewType(position) == RVLoadMoreAdapter.TYPE_LOAD_MORE) {
                    2
                } else {
                    1
                }
            }

        }
    }

    private fun initListener() {
        iv_back.setOnClickListener(mNoDoubleClickListener)

        swipe_layout.setOnRefreshListener {
            if (homeAdapter!!.isLoading) {
                swipe_layout.isRefreshing = false
                return@setOnRefreshListener
            }
            homeAdapter!!.isLoading = true
            downJson(1)
        }

        homeAdapter!!.setLoadingMore(object: RVLoadMoreAdapter.OnLoadingMore{
            override fun onLoadMore() {
                downJson(mCurrentPage+1)
            }

        })
    }

    //防止连续点击
    private val mNoDoubleClickListener: NoDoubleClickListener = object : NoDoubleClickListener() {
        override fun onNoDoubleClick(v: View) {
            when (v.id) {
                R.id.iv_back -> finish()
            }
        }
    }

}