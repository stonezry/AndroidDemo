package com.zhou.demo.adapter

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.zhou.base.adapter.RVLoadMoreAdapter
import com.zhou.base.adapter.RViewHolder
import com.zhou.base.listener.NoDoubleClickListener
import com.zhou.demo.R
import kotlinx.android.synthetic.main.activity_adapter_more.*
import kotlinx.android.synthetic.main.activity_rv_empty.recyclerview
import kotlinx.android.synthetic.main.item_head_layout.*
import java.util.*
import kotlin.collections.ArrayList


class AdapterMoreLoad3Activity : AppCompatActivity() {

    private var homeAdapter: RVLoadMoreAdapter<String>? = null
    private val mDatas: ArrayList<String> = ArrayList()
    private var isFirst = false
    private var mCurrentPage: Int = 0

    private val words = "RecyclerView使用RecyclerView使用RecyclerView使用RecyclerView使用"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
        initListener()
        isFirst = true
    }

    override fun onResume() {
        super.onResume()
        if (isFirst) {
            isFirst = false
            downJson(1)
        }
    }

    private fun downJson(page: Int) {

        Handler().postDelayed(Runnable {
            swipe_layout.isRefreshing = false
            if(page == 1){
                mDatas.clear()
                homeAdapter!!.setLoadMoreComplete(true)
            }
            mCurrentPage = page
            val datalist = ArrayList<String>()

            if(page == 3){ //模拟滑动到最底部
                for (i in 1..7){
                    val x = Random().nextInt(47)
                    datalist.add(words.substring(0,x))
                }
            }else{
                for (i in 1..20){
                    val x = Random().nextInt(47)
                    datalist.add(words.substring(0,x))
                }
            }
            mDatas.addAll(datalist)
            if (datalist.size < 20) {
                homeAdapter!!.setLoadMoreComplete(false)
            }

            homeAdapter!!.notifyDataSetChanged()
            homeAdapter!!.Loading(false)
        },1000)

    }


    private fun initUI() {
        setContentView(R.layout.activity_adapter_more)
        tv_title.text = "加载更多（瀑布流）"

        swipe_layout.isRefreshing = true
        val layoutManager = StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL); //线性布局
        recyclerview.layoutManager = layoutManager
        homeAdapter = object: RVLoadMoreAdapter<String>(this@AdapterMoreLoad3Activity,recyclerview,R.layout.item_rv_txt, mDatas){

            override fun onViewHolderCreated(holder: RViewHolder, itemView: View, viewType: Int) {
                super.onViewHolderCreated(holder, itemView, viewType)
                if (viewType === TYPE_LOAD_MORE) {
                    val lp = holder.itemView.layoutParams
                    if (lp != null && lp is StaggeredGridLayoutManager.LayoutParams) {
                        lp.isFullSpan = true
                    }
                }
            }

            override fun convert(holder: RViewHolder, t: String, position: Int) {
                holder.setText(R.id.txt,mDatas[position])
            }

        }
        recyclerview.itemAnimator = null
        recyclerview.adapter = homeAdapter

        homeAdapter!!.setOnItemClickListener(object: RVLoadMoreAdapter.OnItemClickListener{
            override fun onItemClick(view: View, holder: RecyclerView.ViewHolder, position: Int) {

            }

            override fun onItemLongClick(
                view: View,
                holder: RecyclerView.ViewHolder,
                position: Int
            ): Boolean {
                return false
            }

        })

    }

    private fun initListener() {
        iv_back.setOnClickListener(mNoDoubleClickListener)

        swipe_layout.setOnRefreshListener {
            if (homeAdapter!!.isLoading) {
                swipe_layout.isRefreshing = false
                return@setOnRefreshListener
            }
            homeAdapter!!.isLoading = true
            downJson(1)
        }

        homeAdapter!!.setLoadingMore(object: RVLoadMoreAdapter.OnLoadingMore{
            override fun onLoadMore() {
                downJson(mCurrentPage+1)
            }

        })
    }

    //防止连续点击
    private val mNoDoubleClickListener: NoDoubleClickListener = object : NoDoubleClickListener() {
        override fun onNoDoubleClick(v: View) {
            when (v.id) {
                R.id.iv_back -> finish()
            }
        }
    }

}